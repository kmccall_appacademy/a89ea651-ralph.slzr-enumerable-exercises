require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).

def array_sum(arr)
  arr.reduce(0) { |total, num| total + num }
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
#*make helper function which checks strins for substring*
def sub_string?(long_string, substring)
  ls_array = long_string.split(' ')
  ls_array.include?(substring)
end

def in_all_strings?(long_strings, substring)
  long_strings.all? { |string| sub_string?(string, substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.

#*split string into an array and sort, if letter doesnt matche before or after item dont add to new array*
def non_unique_letters(string)
  letters = string.chars.uniq
  letters.delete(' ')

  letters.select { |ch| string.count(ch) > 1 }
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
#*change string into an array, sort by length, return last two items as an array*
def longest_two_words(string)
  str_array = string.split(' ').sort_by(&:length)
  [str_array[-2],str_array[-1]]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.

# missing_letters('qwert') => ['y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b,'n','m']
#iterate through input string and exists reject it from letters array

#create a letters array variable
#iterate through variable
#reject all letters in given string
def missing_letters(string)
  alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
  alphabet.reject { |ch| string.include?(ch) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
#no_repeat_years(1989, 2001) => [1989]
#no_repeat_years(2010, 2015) => [2013, 2014, 2015]
#*create a helper method that checks if one year has repeats, run method on each year*

#create helper method
#iterate through years and reject which years have repeats
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).reject { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)#2001
  year_array = year.to_s.split('')#['2','0','0','1']
  year_array.uniq != year_array #['2','0','1'] != ['2','0','0','1'] => true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
#one_week_wonders('come','go','go','journey') => ['come', 'journey']
#*create helper method that checks for consecutive repeats, itereate through song list and run helper method*

#iterate through song list
#select songs that dont repeat
def one_week_wonders(songs)
  uniq_songs = songs.uniq
  uniq_songs.select { |song| no_repeats?(song, songs) }
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, idx|
    if song == song_name && songs[idx+1] == song_name
      return false
    end
  end

  return true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.
#for_cs_sake('with a stick of knuckles i cant cope') => 'stick'
#*separate string to an array, make helper function that returns the index location of c, save longest
#location string to a variable, return that variable
def for_cs_sake(string)
  remove_punctuation(string)
  c_words_array = string.split.select { |word| word.downcase.include?('c') }
  return '' if c_words_array.empty?
  c_words_array.sort_by { |word| c_distance(word) }.first
end

def c_distance(word)
  word.reverse.index('c')
end

def remove_punctuation(word)
  word.delete!(',.;?!')
end
# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(numbers)
  ranges = []
  start_index = nil

  numbers.each_with_index do |el, idx|
    next_el = numbers[idx+1]
    if el == next_el
      start_index = idx unless start_index
    elsif start_index
      ranges.push([start_index, idx])
      start_index = nil
    end
  end

  ranges
end
