# EASY

# Define a method that returns an array of only the even numbers in its argument
# (an array of integers).

#get_evens([1,2,3,4,5,6]) => [2,4,6]
#get_evens([4,5,8,2]) => [4,8,2]
#*iterate through the array and save to new array the even numbers*

#iterate through given array
#if current num even save to new array
#return evens array
def get_evens(arr)
  arr.select { |el| el.even? }
end

# Define a method that returns a new array of all the elements in its argument
# doubled. This method should *not* modify the original array.

#calculate_doubles([1,2,3,4]) => [1,4,6,8]
#*iterate through array and double each number*

#create new array variable
#iterate given array
  #multiply current num by 2
  #save to new array
#return double array
def calculate_doubles(arr)
    arr.map { |num| num * 2 }
end

# Define a method that returns its argument with all the argument's elements
# doubled. This method should modify the original array.
def calculate_doubles!(arr)
  arr.map! { |el| num * 2 }
end

# Define a method that returns the sum of each element in its argument
# multiplied by its index.
#array_sum_with_index([2, 9, 7]) => 23 because (2 * 0) + (9 * 1) + (7 * 2) = 0 + 9 + 14 = 23
#*itereate through array and multiple each item by its index then add all products

#iterate through array
  #for each number multiply by its index
  #and then add to a total
def array_sum_with_index(arr)
  total = 0

  arr.each.with_index do |num, idx|
    total += (num * idx)
  end

  total
end

# MEDIUM

# Given an array of bids and an actual retail price, return the bid closest to
# the actual retail price without going over that price. Assume there is always
# at least one bid below the retail price.

#price_is_right([12,4,11,8], 10) => 8
#price_is_right([3,4,11,6], 5) => 4
#*push target price into array, sort array, then return price that comes right before target*

#create sorted array variable
#iterate through array
#when target price is found
#return previous number
def price_is_right(bids, actual_retail_price)
  sorted_array = bids.push(actual_retail_price).sort

  sorted_array.each_with_index do |el, idx|
    if el == actual_retail_price
      return sorted_array[idx-1]
    end
  end

end

# Given an array of numbers, return an array of those numbers that have at least
# n factors (including 1 and the number itself as factors).
# at_least_n_factors([1, 3, 10, 16], 5) => [16] because 16 has five factors (1,
# 2, 4, 8, 16) and the others have fewer than five factors. Consider writing a
# helper method num_factors
#at_least_n_factors([1, 3, 10, 16], 5) => [16]
#*find num of factors for each number and see if it equals the target amount, if so save to new array *

#create helper function that finds the number of factors
#create a new varibale to house all the found numbers
#iterate through array
#find number of factors for each item
#if number matches target save array item to new array
def num_factors(num)
  count = 0
  start_num = num

    while start_num > 0
      if num % start_num == 0
        count += 1
        start_num -= 1
      else
        start_num -= 1
      end
    end

  count
end

def at_least_n_factors(numbers, n)
  at_least_array = []

  numbers.each do |num|
    if num_factors(num) >= n
      at_least_array << num
    end
  end

  at_least_array
end


# HARD

# Define a method that accepts an array of words and returns an array of those
# words whose vowels appear in order. You may wish to write a helper method:
# ordered_vowel_word?
#ordered_vowel_words(['hi','enjoy','youre','vacation']) => ['hi','enjoy','vacation']
#*remove all letters from word that is not a vowel and check for order*

#create helper function that checks if words with just vowels are in order*
#create new array variable
#iterate through array list of words
#run helper function on each word
#if word has vowels in order save to new array variable
def ordered_vowel_words(words)
  ordered_array = []

  words.each do |el|
    if ordered_vowel_word?(el)
      ordered_array << el
    end
  end

  ordered_array
end

def ordered_vowel_word?(word)
  vowels = ['a','e','i','o','u']
  word_vowels = []

  word.each_char do |ch|
    if vowels.include?(ch.downcase)
      word_vowels << ch
    end
  end

  if word_vowels.sort == word_vowels
    return true
  else
    return false
  end
end

# Given an array of numbers, return an array of all the products remaining when
# each element is removed from the array. You may wish to write a helper method:
# array_product.

# products_except_me([2, 3, 4]) => [12, 8, 6], where: 12 because you take out 2,
# leaving 3 * 4. 8, because you take out 3, leaving 2 * 4. 6, because you take out
# 4, leaving 2 * 3

# products_except_me([1, 2, 3, 5]) => [30, 15, 10, 6], where: 30 because you
# take out 1, leaving 2 * 3 * 5 15, because you take out 2, leaving 1 * 3 * 5
# 10, because you take out 3, leaving 1 * 2 * 5 6, because you take out 5,
# leaving 1 * 2 * 3
#*when arrived at a number in an array mulitply all other numbers*

#create a helper function that multiplies all other numbers beside target
#create array variable
#iterate through array
#run helper function on each number and save results to new array
def products_except_me(numbers) #[1,2,3,5]
  products_array = []

  numbers.each do |el|
    products_array << array_product(numbers, el)
  end

  products_array
end

def array_product(array, num) #[1,2,3,5]
  product = 1

  array.each do |el|
    if el != num
      product *= el
    end
  end

  product
end
